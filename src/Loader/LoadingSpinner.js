import styled from "styled-components";
import {CircleLoader} from "react-spinners";
import {useEffect, useState} from "react";

const LoadingSpan = styled.span`
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
    //position: absolute;
    position: relative;
    top: 180%;
    right: 260%;
    //left: 30%;
    transform: translate(-50%, -50%);
    z-index: 999; /* Ensure it's on top of other content */

    @media screen and (max-width: 768px) {
        top: 40%;
    }
    @media screen and (max-width: 480px) {
        top: 30%;
    }
`;

// const Loader = styled(ClipLoader)`
//     margin-right: 5px; /* Adjust margin as needed */
// `;

const LoadingText = styled.span`
    margin-left: 1rem;
    font-size: 13px;
    color: #85d37e;
    font-weight: bold;

`;

const LoadingSpinner = () => {
    const [showSpinner, setShowSpinner] = useState(true);

    useEffect(() => {
        const timer = setTimeout(() => {
            setShowSpinner(false);
        }, 60000);

        return () => clearTimeout(timer); // Clear the timer on unmount
    }, []);

    return showSpinner ? (<LoadingSpan>
        {/*<Loader color="#FFDE02" size={55}/>*/}
        <CircleLoader color="#85d37e" size={60}/>
        <LoadingText>UnityExness......</LoadingText>
    </LoadingSpan>) : null;
};

export default LoadingSpinner;



