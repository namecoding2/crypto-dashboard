export const PostRequest = async (url, headers, body) => {
    try {
        const requestOptions = {
            method: "POST", headers: headers, body: JSON.stringify(body), redirect: "follow"
        };

        const response = await fetch(url, requestOptions);

        if (response.status === 200 || response.status === 201) {
            const data = await response.json();
            console.log(data, "token")
            // setToken(data.token)
            localStorage.setItem('token', data?.token);
            return {status: true, message: 'successful', data};

        } else {
            const errorResponse = await response.json();
            return {status: false, message: errorResponse.message};
        }
    } catch (error) {
        return {status: false, message: error.message || 'An error occurred'};
    }
};