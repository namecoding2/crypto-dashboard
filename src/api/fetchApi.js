const BASEURL = `https://testnet.binance.vision`


export const FetchOrderBook = async (symbol) => {
    try {
        const response = await fetch(`${BASEURL}/api/v3/depth?symbol=${symbol}&limit=100`);
        return await response.json();
    } catch (error) {
        console.error(error);
    }
};


export const FetchData = async (symbol, interval) => {
    try {
        const response = await fetch(`https://api.binance.com/api/v3/klines?symbol=${symbol}&interval=${interval}&limit=500`);
        return await response.json();
    } catch (error) {
        console.error(error);
    }
};

export const FetchTradingTicker = async () => {
    try {
        // const response = await fetch(`https://api.binance.com/api/v3/ticker/24hr`);
        const response = await fetch(`https://testnet.binance.vision/api/v3/ticker/24hr`);
        console.log(response, "response")
        return await response.json();
    } catch (error) {
        console.error(error);
    }
}


export const FetchTicker = async (symbol) => {
    try {
        // const response = await fetch(`https://api.binance.com/api/v3/ticker/24hr?symbol=${symbol}`);
        const response = await fetch(`https://testnet.binance.vision/api/v3/ticker/24hr?symbol=${symbol}`);
        return await response?.json();
    } catch (error) {
        console.error(error);
    }
};

//fetching all the users repository using their username
export const FetchRepo = async (user) => {
    try {
        const response = await fetch(`https://api.github.com/users/${user}/repos`);
        return await response.json();
    } catch (error) {
        console.error(error);
    }
};


// import React from 'react'
//
// const baseUrl = 'https://api.binance.com'
//
// const Price = 'https://api.binance.com/api/v3/ticker/price'
//
//
// export const FetchData = async (symbol, interval) => {
//    try {
//     const response = await fetch(`https://api.binance.com/api/v3/klines?symbol=${symbol}&interval=${interval}&limit=500`);
//     const result = await response.json();
//     return result;
//   } catch (error) {
//     console.error(error);
//   }
// };
//
// export const FetchTradingTicker = async () => {
//   try {
//     const response = await fetch(
//       `https://api.binance.com/api/v3/ticker/24hr`
//     );
//     const result = await response.json();
//     return result;
//   } catch (error) {
//     console.error(error);
//   }
// }
//
//
// // GET /api/v3/ticker/24hr
//
// export const FetchTicker = async (symbol) => {
//   try {
//     const response = await fetch(`${baseUrl}/api/v3/ticker/24hr?symbol=${symbol}`
//     );
//     const result = await response.json();
//     return result;
//   } catch (error) {
//     console.error(error);
//   }
// };
//
//
//
// //fetching all the users repository using their username
// export const FetchRepo = async (user) => {
//   try {
//     const response = await fetch(
//       `https://api.github.com/users/${user}/repos`
//     );
//     const result = await response.json();
//     return result;
//   } catch (error) {
//     console.error(error);
//   }
// };