import {useEffect, useState} from 'react';
import styled from "styled-components";

// const GoogleTranslate = () => {
//     const [translator, setTranslator] = useState(null);
//
//     useEffect(() => {
//         window.ResizeObserver = null
//         // "use strict";
//         const script = document.createElement('script');
//         script.type = 'text/javascript';
//         script.src = 'https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit';
//         script.async = true;
//         document.body.appendChild(script);
//
//         window.googleTranslateElementInit = () => {
//             const translatorInstance = new window.google.translate.TranslateElement({
//                 pageLanguage: 'en',
//                 includedLanguages: 'en,es,fr,it,de,ja',
//                 layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL
//             }, 'google_translate_element');
//             setTranslator(translatorInstance);
//         };
//
//         // Clean up
//         return () => {
//             document.body.removeChild(script);
//         };
//     }, []);
//
//
//     const handleLanguageChange = (languageCode) => {
//         if (translator) {
//             translator.showBanner(false);
//             translator.showOverlay(false);
//             translator.hide();
//
//             setTimeout(() => {
//                 translator.dispose();
//                 const newTranslator = new window.google.translate.TranslateElement({
//                     pageLanguage: 'en',
//                     includedLanguages: languageCode,
//                     layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL
//                 }, 'google_translate_element');
//                 setTranslator(newTranslator);
//             }, 100);
//         }
//     };
//
//     return (<TranslateContainer id="google_translate_element">
//         {/*<LanguageSelect onChange={(e) => handleLanguageChange(e.target.value)}>*/}
//         {/*    <option value="en">English</option>*/}
//         {/*    <option value="es">Spanish</option>*/}
//         {/*    <option value="fr">French</option>*/}
//         {/*</LanguageSelect>*/}
//     </TranslateContainer>);
// };
//
//
// const ChatIconContainer = styled.div`
//     position: fixed;
//     //bottom: 20px;
//     right: 30px;
//     z-index: 999;
//     cursor: pointer;
//
// `;
//
// const TranslateContainer = styled.div`
//     //margin-top: 20px;
//     z-index: 999;
//     cursor: pointer;
//     background: #1C2127;
//
//     .googleElement {
//         color: black;
//         background: #1C2127;
//     }
// `;
//
// const LanguageSelect = styled.select`
//     padding: 4px 12px;
//     border: 1px solid #ccc;
//     border-radius: 4px;
//     font-size: 16px;
// `;
//
// export default GoogleTranslate;



export const GoogleTranslate = () => {
    useEffect(() => {
        const initializeGoogleTranslate = () => {
            if (window.google && window.google.translate) {
                new window.google.translate.TranslateElement({ pageLanguage: 'en' }, 'google_translate_element');

                // Create a MutationObserver to detect when the toolbar elements and iframe are added
                let observer = new MutationObserver(function(mutationsList) {
                    mutationsList.forEach(function(mutation) {
                        mutation.addedNodes.forEach(function(node) {
                            // Check if the added node is a toolbar element
                            if (node.nodeName === 'TR' && node.getAttribute('valign') === 'middle') {
                                // Hide the toolbar element
                                node.style.display = 'none';
                            }
                            // Check if the added node is a div with class skiptranslate containing the iframe
                            else if (node.nodeName === 'DIV' && node.classList.contains('skiptranslate')) {
                                let iframe = node.querySelector('iframe');
                                if (iframe && iframe.id === ':1.container') {
                                    // Hide the iframe
                                    iframe.style.display = 'none';
                                }
                            }
                        });
                    });
                });

                // Start observing changes in the body element
                observer.observe(document.body, { childList: true, subtree: true });

                // Remove the "Powered by Google Translate" text
                let poweredByElement = document.querySelector('a[href="https://translate.google.com"]');
                if (poweredByElement) {
                    let poweredByParent = poweredByElement.closest('td');
                    if (poweredByParent) {
                        poweredByParent.style.display = 'none';
                    }
                }
            }
        };

        // Check if the google object is available
        if (!window.google) {
            const script = document.createElement('script');
            script.src = 'https://translate.google.com/translate_a/element.js?cb=initializeGoogleTranslate';
            script.async = true;
            document.body.appendChild(script);
        } else {
            initializeGoogleTranslate();
        }
    }, []);

    return null; // Render nothing, as this component only performs side effects
};
