import xrp from "../assets/banks/xrp-text-mark-black-1.svg";
import tetherSvg from "../assets/banks/tether.svg";
import xlm from "../assets/banks/tether.svg";
import BitcoinCash from "../assets/banks/bitcoin-sv-1.svg";
import bitcoin from "../assets/banks/bitcoin.svg";
import binance from "../assets/banks/binance.svg";
import litecoin from "../assets/banks/litecoin.svg";
import dash from "../assets/banks/dash-5.svg";
import tron from "../assets/banks/tron.svg";
import Ethereum from "../assets/banks/ethereum.svg";
import shib from "../assets/banks/shibaInu.svg";
import apple from "../assets/banks/apple-card.svg";
import maestro from "../assets/banks/maestro-2.svg";


const walletData = [
    {
        "crypto": {
            "wallets": [
                {
                    "name": "Bitcoin (BTC)",
                    "AccountNumber": "bc1qg8hlwh0qcz7njr9h6329q22knm83tka33g392x",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "btc",
                    "image": <img src={bitcoin} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={bitcoin} alt="Bitcoin"/>
                },
                {
                    "name": "XRP",
                    "AccountNumber": "1234",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "xrp",
                    "image": <img src={xrp} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={xrp} alt="Bitcoin"/>
                },
                {
                    "name": "Tether (USDT TRC)",
                    "AccountNumber": "1234",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "tether",
                    "image": <img src={tetherSvg} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={tetherSvg} alt="Bitcoin"/>
                },
                {
                    "name": "Ethereum",
                    "AccountNumber": "0x2835c3A00Eebd6ee37B52854De829Af87eC6e5fe",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "eth",
                    "image": <img src={Ethereum} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={Ethereum} alt="Bitcoin"/>
                },
                {
                    "name": "Tron",
                    "AccountNumber": "TDXucuGuGDqXL4A982JZP88dAe9r8oEdsn",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "tron",
                    "image": <img src={tron} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={tron} alt="Bitcoin"/>
                },
                {
                    "name": "XLM",
                    "AccountNumber": "GBKZZYSKEQMKNJMXEMET2KVXID2XM2NHGFBW4H2A7H53XIP6RAPLAEOH",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "xlm",
                    "image": <img src={xlm} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={xlm} alt="Bitcoin"/>
                },
                {
                    "name": "XRP",
                    "AccountNumber": "rBGLYUyFX8cJoTxJ6FQr1KXxXTm35aY73Q",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "xrp",
                    "image": <img src={xrp} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={xrp} alt="Bitcoin"/>
                },
                {
                    "name": "Bitcoin Cash",
                    "AccountNumber": "qqxajnlwuxa3r90682kwljuvuchuuhmffczy92rurt",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "btccash",
                    "image": <img src={BitcoinCash} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={BitcoinCash} alt="Bitcoin"/>
                },
                {
                    "name": "BNB",
                    "AccountNumber": "0x2835c3A00Eebd6ee37B52854De829Af87eC6e5fe",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "bnb",
                    "image": <img src={binance} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={binance} alt="Bitcoin"/>
                },
                {
                    "name": "Litecoin",
                    "AccountNumber": "ltc1qmtns22rdhcjjfvdm5euxhzg066v842g73phe4x",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "litecoin",
                    "image": <img src={litecoin} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={litecoin} alt="Bitcoin"/>
                },
                {
                    "name": "Dash Coin",
                    "AccountNumber": "XweWDzyxL5EMU6wnsR1ubmc4Gw2HZRjHVC",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "dash",
                    "image": <img src={dash} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={dash} alt="Bitcoin"/>
                },
                {
                    "name": "USDT ERC 20",
                    "AccountNumber": "0x2835c3A00Eebd6ee37B52854De829Af87eC6e5fe",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "usdterc",
                    "image": <img src={tetherSvg} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={tetherSvg} alt="Bitcoin"/>
                },
                {
                    "name": "USDT TRC 20",
                    "AccountNumber": "TDXucuGuGDqXL4A982JZP88dAe9r8oEdsn",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "usdttrc",
                    "image": <img src={tetherSvg} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={tetherSvg} alt="Bitcoin"/>
                },
                {
                    "name": "Shiba INU",
                    "AccountNumber": "0x2835c3A00Eebd6ee37B52854De829Af87eC6e5fe",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "shiba",
                    "image": <img src={shib} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={shib} alt="Bitcoin"/>
                },
                {
                    "name": "Monero",
                    "AccountNumber": "0x6F6c63514EcDEe67f6fcA95Bd6c464d1471CFeB4",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "monero",
                    "image": <img src={maestro} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={maestro} alt="Bitcoin"/>
                },
                {
                    "name": "Doge Coin",
                    "AccountNumber": "DU6tYwHK91dc81KyfKZPcGrnMVhZdKh8yb",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "minimumDeposit": 100,
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "icon": "doge",
                    "image": <img src={shib} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={shib} alt="Bitcoin"/>
                }
            ],
            "status": "Recommended",
            "type": "coin"
        }
    },
    {
        "Binance Pay": {
            "wallets": [
                {
                    "name": "Binance pay",
                    "AccountNumber": "1234",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "image": <img src={binance} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={binance} alt="Bitcoin"/>
                }
            ],
            "status": "Unavailable",
            "type": "binance"
        }
    },
    {
        "Bank Card": {
            "wallets": [
                {
                    "name": "Bank Card",
                    "AccountNumber": "1234",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "image": <img src={maestro} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={maestro} alt="Bitcoin"/>
                }
            ],
            "status": "Unavailable",
            "type": "bank"
        }
    },
    {
        "Apple Pay": {
            "wallets": [
                {
                    "name": "Apple Pay",
                    "AccountNumber": "1234",
                    "Limit": "Limits 10 - 20,000,000 USD",
                    "fee": "Fee 0%",
                    "processingTime": "ProcessingTime Instant - 30 minutes",
                    "status": true,
                    "image": <img src={apple} alt="Bitcoin"
                                  style={{width: "20px", marginRight: "1rem"}}/>,
                    "Image_Icon": <img src={apple} alt="Bitcoin"/>
                }
            ],
            "status": "Unavailable",
            "type": "apple"
        }
    }
]


export const recommendedWallets = walletData.filter(wallet => wallet[Object.keys(wallet)].status === "Recommended")[0]?.[Object.keys(walletData.filter(wallet => wallet[Object.keys(wallet)].status === "Recommended")[0])]?.wallets;

export const unavailableWallets = walletData.filter(wallet => wallet[Object.keys(wallet)].status === "Unavailable").map(wallet => wallet[Object.keys(wallet)].wallets).flat();

