import React, {useState} from 'react'
import styled from 'styled-components';
import {FaRegUserCircle} from "react-icons/fa";


const Profile = ({IsCheck, onCancel}) => {

    const [isOpen, setIsOpen] = useState(false);
    const [isClose, setIsClose] = useState(false);


    const toggleDropdown = () => {
        setIsOpen(!isOpen);
        setIsClose(false);
        IsCheck(false);
        // onCancel(onCancel)
        onCancel && onCancel();
    };

    const toggleDropdowns = () => {
        setIsClose(!isClose);
        setIsOpen(false);
        // onCancel(onCancel)
        // Check if onCancel exists before calling it
        onCancel && onCancel();
    };


    return (<div>

            <DropdownContainer>
                <DropdownButton onClick={toggleDropdowns}>
                    <h3> $0.00 USD</h3>
                </DropdownButton>

                <DropdownButton>
                    <FaRegUserCircle onClick={toggleDropdown} className='icon'/>
                </DropdownButton>


                <DropdownContent isClose={isClose}>
                    <DropdownItem href="#">
                        <span>Balance</span>
                        <span>10,0000</span>
                        <span>(?)</span>
                    </DropdownItem>

                    <DropdownItem href="#">
                        <span>Equity</span>
                        <span>10,0000</span>
                        <span>(?)</span>
                    </DropdownItem>

                    <DropdownItem href="#">
                        <span>Margin</span>
                        <span>10,0000</span>
                        <span>(?)</span>
                    </DropdownItem>

                    <DropdownItem href="#">
                        <span>Free margin</span>
                        <span>10,0000</span>
                        <span>(?)</span>
                    </DropdownItem>

                    <DropdownItem href="#">
                        <span>Free level</span>
                        <span>10,0000</span>
                        <span>(?)</span>
                    </DropdownItem>

                </DropdownContent>


                <DropdownContent isOpen={isOpen}>
                    <DropdownItem href="#">Deposit</DropdownItem>
                    <DropdownItem href="#">support</DropdownItem>
                    <DropdownItem href="#">suggest a feature</DropdownItem>
                    <hr/>
                    <DropdownItem href="#">sign out</DropdownItem>
                </DropdownContent>


            </DropdownContainer>

        </div>


    )
}


const DropdownContainer = styled.div`
    position: relative;
    display: inline-block;

    .icon {
        font-size: 35px;
        padding-top: 5px;
    }
`;


const DropdownButton = styled.button`
    background-color: transparent;
    color: white;
    padding: 16px;
    font-size: 15px;
    border: none;
    cursor: pointer;

    .icon {
        font-size: 30px;
        padding-top: 5px;
        color: white;
    }

    h3 {
        font-size: 25px;
    }

    &:active {
        color: #FFCF01;
    }

    &:hover {
        background-color: #20252B;
        color: #FFCF01;
    }


    @media (max-width: 768px) {
        h3 {
            font-size: 10px;
        }
    }

    @media (max-width: 480px) {
        padding: 0px;
        &:hover {
            background: none;
            color: #FFCF01;
        }

        .icon {
            font-size: 33px;
            //padding-top: 8px;
        }

        h3 {
            display: none;
        }
    }
`;

const DropdownContent = styled.div`
    display: ${props => (props.isOpen || props.isClose ? 'block' : 'none')};
    position: absolute;
    background-color: #1C2227;
    padding: 5px;
    min-width: 270px;
    margin-top: -12px;
    //box-shadow: 0px 8px 16px 0px rgba(154, 80, 80, 0.2);
    //drop-shadow: 0px 8px 16px 0px rgba(154, 80, 80, 0.2);
    z-index: 99;
    left: auto;
    right: 0;

    @media (max-width: 768px) {
        min-width: 200px;
        margin-top: 7.5rem;
    }

    @media (max-width: 480px) {
        min-width: 250px;
        margin-top: 1rem;
        margin-right: -3rem;
        left: auto;
        right: 0;
    }
`;

const DropdownItem = styled.a`
    color: #FFFFFF;
    padding: 13px 20px;
    text-decoration: none;
    display: flex;
    justify-content: space-between;
    text-align: start;
    font-size: 12px;

    span {
        font-size: 11px;
        text-align: start;
    }

    &:hover {
        //background-color: #20252B;
        color: #FFCF01;
    }

`;


export default Profile