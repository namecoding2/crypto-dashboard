import React from 'react'
import styles from './molecules.module.css'
import arrow from '../../assets/images/icons/icons/Arrow Up 2/greenArrow.svg'

function Table({orderBookData, pairTicker}) {

    console.log(orderBookData)
    return (<div>
        <div className={styles.table}>
            <table>
                <thead>
                <tr>
                    <th>Price (USDT)</th>
                    <th>lastQty</th>
                    {/*<th>Total</th>*/}
                </tr>

                </thead>

                <tbody>
                {orderBookData?.asks?.slice(0, 6).map((bid, index) => (<tr className={styles.after1} key={index}>
                    <td style={{paddingRight: "1rem"}}>{bid[0]}</td>
                    <td>{bid[1]}</td>
                </tr>))}

                </tbody>


            </table>
        </div>


        <div className={styles.summaryDem}>
            <h1 className="ask">{pairTicker?.askPrice}</h1>

            <img
                src={arrow} alt='arrow'
            />
            <h1 className="">{pairTicker?.volume}</h1>
        </div>

        <div className={styles.table2}>

            <table>
                <tbody>
                {orderBookData?.bids?.slice(0, 6).map((bid, index) => (<tr className={styles.before1} key={index}>
                    <td style={{paddingRight: "1rem"}}>{bid[0]}</td>
                    <td>{bid[1]}</td>
                </tr>))}

                {/*<tr className={styles.before1}>*/}
                {/*    <td>36920.12</td>*/}
                {/*    <td>0.758965</td>*/}
                {/*    <td>28,020.98</td>*/}
                {/*</tr>*/}

                {/*<tr className={styles.before2}>*/}
                {/*    <td>36920.12</td>*/}
                {/*    <td>0.758965</td>*/}
                {/*    <td>28,020.98</td>*/}
                {/*</tr>*/}
                </tbody>
            </table>
        </div>
    </div>)
}

export default Table