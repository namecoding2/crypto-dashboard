import React, {useState} from 'react'
import logo from "../../assets/unity.png";
import {CgMenuGridO} from "react-icons/cg";
import {MdOutlineAddchart, MdOutlineCandlestickChart, MdSupportAgent} from "react-icons/md";
import {FcCurrencyExchange} from "react-icons/fc";
import {HiUsers} from "react-icons/hi2";
import {FaRegUserCircle} from "react-icons/fa";
import {IoIosNotificationsOutline} from "react-icons/io";
import styled, {keyframes} from "styled-components";
import {Link, useNavigate} from "react-router-dom";
import {HiOutlineMenu} from "react-icons/hi";
import {DropdownButton} from "../organisms/navbar";
import {AiOutlineGlobal} from "react-icons/ai";
import {useAuth} from "../../contexts/AppContext";
import {BiLogOut} from "react-icons/bi";
import {PiCurrencyBtcFill} from "react-icons/pi";
import {CiWallet} from "react-icons/ci";
import {formatCurrency} from "./TABS/formatCurrency";
import 'react-toastify/dist/ReactToastify.css';




const Header = ({OpenSidebar}) => {
    const navigate = useNavigate();
    const [isOpen, setIsOpen] = useState(false);
    const [isWallet, setIsWallet] = useState(false);
    const [isProfile, setIsProfile] = useState(false);
    const [isNotification, setIsNotification] = useState(false);

    const {Logout, token, isLoggedIn, user, setIsLoggedIn, setToken,setUser, setInvestmentPlan, setTransactionHistory, setInvestmentHistory} = useAuth();


    const toggleDropdown = () => {
        setIsOpen(!isOpen);
        setIsProfile(false)
        setIsWallet(false);

    };

    const toggleWallet = () => {
        setIsWallet(!isWallet);
        setIsOpen(false);
        setIsProfile(false)
    };


    const toggleProfile = () => {
        setIsProfile(!isProfile)
        setIsWallet(false);
        setIsOpen(false);
    };


    const toggleNotification = () => {
        setIsNotification(!isNotification)
        setIsProfile(false)
        setIsWallet(false);
        setIsOpen(false);
    }


    const handleLogout = async () => {
        setIsLoggedIn(false);
        setToken(null);
        localStorage.removeItem('token');
        localStorage.removeItem('isLoggedIn');
        try {
           await Logout()
            navigate('/');
        } catch (error) {
            console.error('Error logging out:', error);
        }
    };
    
    


    // const amount = user?.balance;
    const formattedAmount = formatCurrency(user?.balance);


    return (

        <header className='header'>
            <div className='menu-icon'>
                <HiOutlineMenu className='open-icon' onClick={OpenSidebar}/>
            </div>

            <div className='header-left'>
                <img src={logo}
                     alt="fictional logo"
                     className='icon'
                     style={{width: "100px", marginTop: "0.5rem"}}
                />
            </div>

            <div className='header-right'>
                <DropdownButton className="hidden" onClick={toggleWallet}>
                    <p><CiWallet/> {formattedAmount} USD</p>
                </DropdownButton>


                <AiOutlineGlobal className='Nicon hidden'/>

                <CgMenuGridO className='Nicon' onClick={toggleDropdown}/>

                <IoIosNotificationsOutline className='Nicon' onClick={toggleNotification}/>

                <FaRegUserCircle className='Nicon' onClick={toggleProfile}/>

                <DropdownContent isNotification={isNotification}>
                    <DropdownContentWrapper>
                        <div className="item-div" style={{padding: "1.5rem"}}>
                            <p>Now you can open a fund and attract</p>
                            <p> investments from investors</p>
                            <span>{new Date(Date.now()).toLocaleString()}</span>

                        </div>
                    </DropdownContentWrapper>
                </DropdownContent>


                <DropdownContent isWallet={isWallet}>
                    <DropdownContentWrapper>
                        <div className="item-div">
                            {/*<p className="item-text-Amount">Wallet</p>*/}
                            <p className="">{formattedAmount} USD</p>
                            <p className="item-text-ID">Investment wallet</p>
                            <p className="item-text-ID">#{user?.walletNumber}</p>

                        </div>
                    </DropdownContentWrapper>
                </DropdownContent>

                <DropdownContent isProfile={isProfile}>
                    {/*<DropdownContentWrapper>*/}
                    <div>
                        <div className="item-user"
                           style={{display: "flex", flexDirection: "row", padding: "15px"}}>
                            <FaRegUserCircle style={{fontSize: "18px"}}/>
                            <div style={{marginLeft: "10px"}}>
                                <p> {user?.firstName} {user?.lastName}</p>
                                <p style={{fontSize: "10px", color: "gray"}}> {user?.email}</p>
                            </div>
                        </div>

                    </div>
                    <hr/>
                    <div className="item">

                        <Link to="/profile/deposit">
                            <p className="item-text">
                                <PiCurrencyBtcFill
                                    style={{
                                        paddingTop: "6px",
                                        fontSize: "20px",
                                        marginRight: "3px"
                                    }}/>
                                Deposit</p>
                        </Link>
                        <p className="item-text">
                            <MdSupportAgent
                                style={{paddingTop: "6px", fontSize: "20px", marginRight: "3px"}}/>
                            Support</p>

                    </div>
                    <hr/>
                    <div className="item">

                        <p className="item-text" onClick={handleLogout}>
                            <BiLogOut
                                style={{paddingTop: "6px", fontSize: "20px", marginRight: "3px"}}/>
                            Sign Out
                        </p>
                    </div>
                    {/*</DropdownContentWrapper>*/}
                </DropdownContent>

                <DropdownContent isOpen={isOpen}>
                    <DropdownContentWrapper>
                        <div>
                            <DropdownItem>
                                <Link to="/profile">
                                    <MdOutlineAddchart className='icons'/>
                                    <br/>
                                    <span className='span'>Personal Area</span>
                                </Link>
                            </DropdownItem>
                            <DropdownItem>
                                <Link to="#">
                                    <MdOutlineCandlestickChart className='icon'/>
                                    <br/>
                                    <span className='span'>Public Website</span>
                                </Link>
                            </DropdownItem>
                        </div>
                        <div>
                            <DropdownItem>
                                <Link to="/dashboard">
                                    <FcCurrencyExchange className='icons'/>
                                    <br/>
                                    <span className='span'>Trade Analytics</span>
                                </Link>
                            </DropdownItem>
                            <DropdownItem>
                                <Link to="#">
                                    <HiUsers className='icon'/>
                                    <br/>
                                    <span className='span'>Partnership</span>
                                </Link>
                            </DropdownItem>
                        </div>
                    </DropdownContentWrapper>
                </DropdownContent>
            </div>


        </header>)
}

export default Header

const fadeIn = keyframes`
    from {
        opacity: 0;
        transform: translateY(-50px);
    }
    to {
        opacity: 1;
        transform: translateY(0);
    }
`;

const fadeOut = keyframes`
    from {
        opacity: 1;
        transform: translateY(0);
    }
    to {
        opacity: 0;
        transform: translateY(-50px);
    }
`;


const DropdownContent = styled.div`
    animation: ${(props) => (props.isOpen || props.isWallet || props.isProfile || props.isNotification ? fadeIn : fadeOut)} 0.8s ease;
    visibility: ${props => (props.isOpen || props.isWallet || props.isProfile || props.isNotification ? 'visible' : 'hidden')};
    position: absolute;
    //background-color: #141D22;
    background-color: #ffffff;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    min-width: 300px;
    z-index: 99;
    //margin-top: 1px;
    cursor: pointer;
    //left: auto;
    right: 8px;
    top: 6.5rem;
    border-radius: 5px;


    .item-div {
        padding: 13px;
        font-size: 12px;
        color: #000000;
        //color: #FFFFFF;
    }

    .item {
        padding: 0.2rem 0;
    }

    hr {
        color: #FFFFFF
    }

    .item-user {
        padding: 6px 1.1rem;
        font-size: 13px;
        color: #000000;
    }

    .item-text {
        padding: 6px 1rem;
        font-size: 13px;
        color: #000000;
    }

    .item-text:hover {
        //color: #f1c40f;
        background-color: #DFE4E7

    }

    .item-text-ID {
        padding: 5px;
        //color: #9e9ea4;
        color: #000000;
    }

    .item-text-Amount {
        font-weight: 700;
        padding: 5px;
        font-size: 16px;
    }

    @media (max-width: 768px) {
        min-width: 200px;
        margin-top: 0.5rem;
    }

    @media (max-width: 480px) {
        min-width: 250px;
        margin-top: 1rem;
        z-index: 99;
        right: 0;
    }
`;

const DropdownContentWrapper = styled.div`
        // animation: ${(props) => (props.isOpen ? fadeIn : fadeOut)} 0.5s ease;
        // visibility: ${props => (props.isOpen || props.isWallet || props.isProfile ? 'visible' : 'hidden')};
        //animation: ${(props) => (props.isOpen ? fadeIn : fadeOut)} 0.5s ease;
    display: flex;
    cursor: pointer;
    width: 100%;
    color: #000000;
    //flex-direction: column;

    a {
        color: #000000;
    }

    .item {
        padding: 1rem;
        color: #000000;
    }

    .item-text {
        padding: 8px;
        font-size: 13px;
        color: #000000;
    }

    .item-text:hover {
        color: #f1c40f;

    }
`;

const DropdownItem = styled.div`
    //color: #FFFFFF;
    color: #000000;
    //background-color: #000000;
    padding: 20px 30px;
    text-decoration: none;
    text-align: center;
    display: block;
    cursor: pointer;

    .span {
        font-size: 12px;
    }

    .span:hover {
        color: #FFCF01;
    }

    .icon {
        font-size: 20px;
    }

    &:hover {
        //background-color: #20252B;
        color: #FFCF01;
    }

`;

