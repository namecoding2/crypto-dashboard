import React from 'react'
import styled from "styled-components";
import Tab, {MainContainer} from "../TABS/Tabs";
import {Wrapper} from "./Account";
import {InvestmentPlanCard, InvestmentWithdrawalForm} from "../TABS/InvestmentPlans";
import EmptyImage from "../TABS/EmptyImage";
import withLoader from "../../../Loader/WithLoader";
import {InvestmentHistory} from "../TABS/TransactionHistory/InvestmentHistory";

import {formatCurrency} from "../TABS/formatCurrency";
import 'react-toastify/dist/ReactToastify.css';
import {useAuth} from "../../../contexts/AppContext";

const InvestmentWallet = () => {
    const {InvestmentPlan, user, investmentHistory, RedeemInvestment} = useAuth();


    // console.log(InvestmentPlan, "history");

    const tabs = [{
        title: "Investment Plan", content: <MainContainer>
            {/*<ToastContainer/>*/}
            <h2 className='header-text'>Investment Plan</h2>
            <p className='header-text'>See all Investment Plan.</p>
            <PlansContainer>

                {InvestmentPlan?.investmentPlans?.map((plan, index) => (
                <InvestmentPlanCard
                        key={plan._id}
                        _id={plan._id}
                        planName={plan.planName}
                        planType={plan.planType}
                        desc={plan.desc}
                        minimumInitialDeposit={plan.minimumInitialDeposit}
                        maximumInitialDeposit={plan.maximumInitialDeposit}
                        spread={plan.spread}
                        commission={plan.commission}
                        duration={plan.duration}
                        planStatus={plan.planStatus}
                    />

                ))}
            </PlansContainer>
        </MainContainer>
    }, {
        title: "Withdraw Investment ", content: <MainContainer>
            {/*<ToastContainer/>*/}
            <p className='header-text'>See all Withdrawal payment methods.</p>
            <InvestmentWithdrawalForm user={user} RedeemInvestment={RedeemInvestment}/>
        </MainContainer>
    }, {
        title: "My Investment", content: <MainContainer>
            <p className='header-text'>See all Investment transaction history.</p>

            {investmentHistory && investmentHistory.length > 0 ? (
                <InvestmentHistory investmentHistory={investmentHistory}/>) : (<EmptyImage/>)}

        </MainContainer>
    },


    ];

    const amount = user?.investBalance;
    const formattedAmount = formatCurrency(amount);

    return (<main className='main-container'>
        {/*<ToastContainer/>*/}
        <Wrapper>
            <div style={{display: "flex", justifyContent: "space-between"}}>
                <div style={{marginTop: "1rem"}}>
                    <h4>Manage Investment</h4>
                    <p>Use the funds in this wallet for your investments</p>

                </div>
                <div style={{marginTop: "1rem"}}>
                    <span className="balance-text">Investment Profit</span>
                    <h4 className="balance">{formattedAmount}</h4>
                </div>
            </div>


            <Tab tabs={tabs}/>

        </Wrapper>
    </main>)
}


// export default InvestmentWallet
export default withLoader(InvestmentWallet);


export const GridContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(380px, 1fr));
    gap: 10px;
    //width: 100%;
`;


const PlansContainer = styled.div`
    display: flex;
    overflow-x: auto;
    padding: 20px 0;
`;