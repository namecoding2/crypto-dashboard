import React from 'react'
import {Link} from "react-router-dom";
import {BsGrid1X2Fill} from "react-icons/bs";
import {TbCircleArrowDown, TbPresentationAnalytics, TbSettings2} from "react-icons/tb";
import {PiArrowCircleUpRightBold} from "react-icons/pi";
import {MdHistory} from "react-icons/md";
import {LuWallet} from "react-icons/lu";
import {GoBriefcase} from "react-icons/go";
import 'react-toastify/dist/ReactToastify.css';

const DashboardSidebar = ({openSidebarToggle}) => {
    // const Sidebar = ({openSidebarToggle, OpenSidebar}) => {

    return (

        <aside id="sidebar" className={openSidebarToggle ? "sidebar-responsive" : ""}
        style={{backgroundColor:"#121521"}}
        >
            {/*<div className='sidebar-title'>*/}
            {/*    <IoMdCloseCircleOutline className='icon close_icon' onClick={OpenSidebar}/>*/}
            {/*</div>*/}
            <ul className='sidebar-list'>
                <Link to="/profile" >
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <BsGrid1X2Fill className='icon' style={{color:"#fff"}}/>
                        My accounts

                    </li>
                </Link>

                <Link to="/profile/deposit">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <TbCircleArrowDown className='icon' style={{color:"#fff"}}/>
                        Deposit
                    </li>
                </Link>

                <Link to="/profile/withdrawal">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <PiArrowCircleUpRightBold className='icon' style={{color:"#fff"}}/>
                        Withdrawal

                    </li>
                </Link>

                <Link to="/profile/transactions">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>

                        <MdHistory className='icon' style={{color:"#fff"}}/>
                        Transaction history
                    </li>
                </Link>

                <Link to="/profile/investment">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <LuWallet className='icon' style={{color:"#fff"}}/>
                        Investment
                    </li>
                </Link>

                <Link to="/profile/setting">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <TbSettings2 className='icon' style={{color:"#fff"}}/>
                        Settings
                    </li>
                </Link>


                <Link to="/dashboard">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <TbPresentationAnalytics className='icon' style={{color:"#fff"}}/>
                        Analytics
                    </li>
                </Link>
                <Link to="/profile/portfolio">
                    <li className='sidebar-list-item' style={{color:"#fff"}}>
                        <GoBriefcase className='icon' style={{color:"#fff"}}/>
                        Portfolio management

                    </li>
                </Link>
            </ul>

        </aside>)
}

export default DashboardSidebar