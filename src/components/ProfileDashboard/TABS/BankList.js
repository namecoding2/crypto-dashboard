import tetherSvg from "../../../assets/banks/tether.svg";
import xlm from "../../../assets/banks/tether.svg";
import BitcoinCash from "../../../assets/banks/bitcoin-sv-1.svg";
import bitcoin from "../../../assets/banks/bitcoin.svg";
import binance from "../../../assets/banks/binance.svg";
import litecoin from "../../../assets/banks/litecoin.svg";
import dash from "../../../assets/banks/dash-5.svg";
import tron from "../../../assets/banks/tron.svg";
import xrp from "../../../assets/banks/xrp-text-mark-black-1.svg";
import Ethereum from "../../../assets/banks/ethereum.svg";
import shib from "../../../assets/banks/shibaInu.svg";
import apple from "../../../assets/banks/apple-card.svg";
import maestro from "../../../assets/banks/maestro-2.svg";


export const Banks = [{
    session: "Recommended",
    name: "XRP",
    type: "coin",
    AccountNumber: "hyy54895489g4h9459835f30",
    processingTime: " processingTime Instant - 30 minutes",
    fee: "Fee 0%",
    Limit: "Limits 3 - 10,000 USD",
    image: <img src={xrp} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
    Image_Icon: <img src={xrp} alt="Bitcoin"/>
}, {
    session: "Recommended",
    name: "Tether (USDT TRC)",
    type: "coin",
    AccountNumber: "kwldjek4387s8ud8dfdfyddfdf",
    processingTime: " processingTime Instant - 1 day",
    fee: "Fee 0%",
    Limit: "Limits 10 - 10,000,000 USD",
    image: <img src={tetherSvg} alt="usdt" style={{width: "20px", marginRight: "1rem"}}/>,
    Image_Icon: <img src={tetherSvg} alt="usdt"/>
}, {
    session: "Recommended",
    name: "Bitcoin (BTC)",
    type: "coin",
    AccountNumber: "w7w9fdf8f8f7d76f7f7f5f78ff9",
    processingTime: " processingTime Instant - 30 minutes",
    fee: "Fee 0%",
    Limit: "Limits 3 - 10,000,000 USD", // image: bankCardSvg,
    image: <img src={bitcoin} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
    Image_Icon: <img src={bitcoin} alt="Bitcoin"/>
},

    {
        session: "Recommended",
        name: "Ethereum",
        type: "coin",
        AccountNumber: "w0a889afa87a88a8a8b8f787bf",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={Ethereum} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={Ethereum} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "Tron",
        type: "coin",
        AccountNumber: "daiad8d87duyfufd9uffdkd00d",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={tron} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={tron} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "XLM",
        type: "coin",
        AccountNumber: "daoaa09df899a89fduafdd99df9",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={xlm} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={xlm} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "XRP",
        type: "coin",
        AccountNumber: "sfaoaa09a89faaifd89df99df",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={xrp} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={xrp} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "Bitcoin Cash",
        type: "coin",
        AccountNumber: "aodf9df8df7fdyu8fd8ff7d77d",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={BitcoinCash} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={BitcoinCash} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "BNB",
        type: "coin",
        AccountNumber: "faofn95876v287572viwjt0t5v",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={binance} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={binance} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "Litecoin",
        type: "coin",
        AccountNumber: "2958n9v85uv8575n9v859vn45245",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={litecoin} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={litecoin} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "DashCoin",
        type: "coin",
        AccountNumber: "2v5ov5n95unvu52985958985",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={dash} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={dash} alt="Bitcoin"/>
    }, {
        session: "Recommended",
        name: "USDT ERC 20",
        type: "coin",
        AccountNumber: "voutivn2utv8u59549589842",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={tetherSvg} alt="usdt" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={tetherSvg} alt="usdt"/>
    }, {
        session: "Recommended",
        name: "Shiba INU",
        type: "coin",
        AccountNumber: "vnitiuv985u42989545254",
        processingTime: " processingTime Instant - 30 minutes",
        fee: "Fee 0%",
        Limit: "Limits 3 - 10,000 USD",
        image: <img src={shib} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
        Image_Icon: <img src={shib} alt="Bitcoin"/>
    },

];


export const UnavailableBanks = [{
    session: "Unavailable",
    name: "Binance Pay",
    type: "coin",
    AccountNumber: "s0sd9s0fuf9f9f9f7f8k7f78",
    processingTime: " processingTime Instant - 30 minutes",
    fee: "Fee 0%",
    Limit: "Limits 10 - 20,000,000 USD",
    image: <img src={binance} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
    Image_Icon: <img src={binance} alt="Bitcoin"/>
}, {
    session: "Unavailable",
    name: "Bank Card",
    type: "bank",
    AccountNumber: "tiojnvou985u9u54928u59589u",
    processingTime: " processingTime Instant - 30 minutes",
    fee: "Fee 0%",
    Limit: "Limits 3 - 10,000 USD",
    image: <img src={maestro} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
    Image_Icon: <img src={maestro} alt="Bitcoin"/>
}, {
    session: "Unavailable",
    name: "apple Pay",
    type: "bank",
    AccountNumber: "v5niu2985u98uviuy02u05uuuv",
    processingTime: " processingTime Instant - 30 minutes",
    fee: "Fee 0%",
    Limit: "Limits 3 - 10,000 USD",
    image: <img src={apple} alt="Bitcoin" style={{width: "20px", marginRight: "1rem"}}/>,
    Image_Icon: <img src={apple} alt="Bitcoin"/>
},


];
