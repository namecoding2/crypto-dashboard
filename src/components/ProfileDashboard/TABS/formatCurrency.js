
export const formatCurrency = (amount) => {
    const numberValue = Number(amount || "");
    return numberValue?.toLocaleString('en-US', {
        style: 'currency', currency: 'USD',
        // minimumFractionDigits: 2,
        // maximumFractionDigits: 2
    });

};


export const formatCurrencies = (amount, profit) => {
    const formattedAmount = amount.toLocaleString('en-US', {
        style: 'currency', currency: 'USD'
    });

    const formattedProfit = profit.toLocaleString('en-US', {
        style: 'currency', currency: 'USD'
    });

    return [formattedAmount, formattedProfit];
};


export function truncateText(text, maxLength = 20) {
    if (text.length > maxLength) {
        return text.substring(0, maxLength) + '...';
    }
    return text;
}

