import React, {useState} from "react";
import ReactPaginate from "react-paginate";
import styled from "styled-components";


const Pagination = ({pageCount, onPageChange}) => {
    const [currentPage, setCurrentPage] = useState(0);

    const handlePageClick = (data) => {
        const selectedPage = data.selected;
        setCurrentPage(selectedPage);
        onPageChange(selectedPage);
    };

    return (<PaginationContainer>
        <ReactPaginate
            pageCount={pageCount}
            onPageChange={handlePageClick}
            forcePage={currentPage}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            previousLabel={"Previous"}
            nextLabel={"Next"}
            breakLabel={"..."}
            containerClassName={"pagination"}
            activeClassName={"active"}
        />
    </PaginationContainer>);
};

const PaginationContainer = styled.div`
    .pagination {
        display: flex;
        list-style: none;
        padding: 0;
        justify-content: center;
        margin-top: 20px;

        li {
            margin: 0 5px;
            cursor: pointer;
            user-select: none;
            padding: 5px 12px;
            border: 1px solid #ccc;
            border-radius: 3px;
            color: #141D22;
            background-color: #141D22;
            font-size: 12px;

            &.active {
                background-color: #e7cc03;
                color: #141D22;

            }

            &:hover {
                background-color: #FFDE02;
                color: #141D22;
            }
        }

        .break {
            cursor: not-allowed;
        }
    }
`;

export default Pagination;
