import React from 'react';
import styled from 'styled-components';
import pcs from "../../../../assets/pcs.png"
import pc from "../../../../assets/pc.png"


// Product Specifications
export const PortFolio = () => {
    return (

        <Container>
            <h2>How allocation works</h2>
            <hr className='hr'/>
            <InnerContainer>
                <Box className="header-text">
                    <h5>Capital you trade</h5>
                </Box>
                <Box>
                    <p>
                        Deposit your own money into a strategy account and trade using that capital
                    </p>
                </Box>
                <Box><p>
                    Trade on a fund account using the pooled capital of all your investors
                </p></Box>
            </InnerContainer>
            <InnerContainer>
                <Box className="header-text">
                    <h5>Order allocation</h5>
                </Box>
                <Box>
                    <p>
                        Orders are copied to each investment using a copy coefficient (calculated as follows: investment
                        equity divided by your equity)
                    </p>
                </Box>
                <Box><p>
                    Orders are allocated to each investment according to its equity share (calculated as follows:
                    investment
                    equity divided by the sum of all investments)
                </p></Box>
            </InnerContainer>
            <InnerContainer>
                <Box className="header-text">
                    <h5>Minimum lot size that can be allocated to investments</h5>
                </Box>
                <Box><p>0.0000001
                    Even small orders will be allocated to the investments of all your investors</p></Box>
                <Box><p>0.0001
                    A larger number of investments or larger difference between their equity requires a larger order to
                    be
                    allocated to all investments</p></Box>
            </InnerContainer>
            <h2>How to invite investors</h2>
            <hr className='hr'/>
            <InnerContainer>
                <Box className="header-text">
                    <h5>Requirements to start inviting investors</h5>
                </Box>
                <Box><p> Deposit 500 USD
                    Significant Trading Reliability Level

                    More about Trading Reliability Level</p></Box>
                <Box><p> High and significant Trading Reliability Level

                    More about Trading Reliability Level</p></Box>


                <Box className="header-text">
                    <h5>Scale business</h5>
                </Box>
                <Box> __ </Box>
                <Box><p>Reach more potential investors with the help of an associate. An automated system will handle
                    all calculations and payouts</p></Box>
            </InnerContainer>

            <h2>Maximum investments permitted</h2>
            <hr className='hr'/>
            <InnerContainer>
                <Box className="header-text">
                    <h5>Total equity</h5>
                </Box>
                <Box><p>≤ 200 000 USD
                    Upon reaching the maximum amount, the strategy will become unavailable for investors to invest in
                    and join</p></Box>
                <Box><p>Unlimited</p></Box>
            </InnerContainer>


            <h2>Investment management rules</h2>
            <hr className='hr'/>
            <InnerContainer>
                <Box className="header-text">
                    <h5>Manual start of allocation</h5>
                </Box>
                <Box><p>Available</p></Box>
                <Box><p>Available</p></Box>


                <Box className="header-text">
                    <h5>Which orders are allocated to the investment</h5>
                </Box>
                <Box><p>New and open orders are copied to the investment.</p></Box>
                <Box><p>Only new orders are allocated to the investment.</p></Box>


                <Box className="header-text">
                    <h5>Hide copied orders from investors</h5>
                </Box>
                <Box><p>Not available</p></Box>
                <Box><p>Available</p></Box>


                <Box className="header-text">
                    <h5>Close investment</h5>
                </Box>
                <Box><p>Instant by investor request</p></Box>
                <Box><p>You have 36 hours after an investor's request to close the investment manually</p></Box>
            </InnerContainer>


        </Container>);
};


const Container = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: start;
    justify-content: start;
    //height: 100vh;

    .hr {
        height: 1px;
        width: 100%;
        color: rgba(255, 255, 255, 0.31);
    }

    @media (max-width: 768px) {
        h2 {
            color: #ffffff;
        }

        .header-text {
            display: none;
        }
    }
`;

const InnerContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: start;
    align-items: start;
    margin-top: 1.5rem;
`;

const Box = styled.div`
    width: 30%;
    //width: 200px;
    //height: 200px;
    margin: 8px;
    background-color: ${({color}) => color};

    p {
        font-size: 13px;
        color: #1C2127;
    }

    @media (max-width: 768px) {
        width: 162px;
        //height: 130px;
        margin: 1px 6px;
        p {
            font-size: 15px;
            color: #ffffff;
        }
    }
`;


// Product Overview

export const PortFolioCopy = () => {
    return (<CardContainer>
        <ResponsiveCard>
            <Image src={pc} alt="Placeholder"/>
            <Text>
                For big number of small investors
            </Text>
            <BulletList>
                <BulletListItem>
                    Deposit your own money into a strategy account and trade using that capital
                </BulletListItem>
                <BulletListItem>
                    Orders are copied to each investor's invested capital using a copy
                    coefficient
                </BulletListItem>
                <BulletListItem>
                    The total equity is limited to 200,000 USD
                </BulletListItem>
            </BulletList>
            <Button>Select Portfolio Copying</Button>
        </ResponsiveCard>


        <ResponsiveCard>
            <Image src={pcs} alt="Placeholder"/>
            <Text>
                For pooled funds of several large investors
            </Text>
            <Text>
                Be responsible for investment performance
            </Text>
            <BulletList>
                <BulletListItem>
                    Trade on a fund account using the pooled capital of all your investors
                </BulletListItem>
                <BulletListItem>
                    Orders are allocated to each investment according to its equity share
                </BulletListItem>
                <BulletListItem>
                    The total equity is unlimited
                </BulletListItem>
            </BulletList>
            <Button>Select Portfolio Management</Button>
        </ResponsiveCard>
    </CardContainer>);
};


const CardContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`;

const ResponsiveCard = styled.div`
    width: calc(50% - 20px); /* 50% width with margin */
    margin-bottom: 20px;

    @media (max-width: 768px) {
        width: 100%; /* Full width on small screens */
    }
`;

const Image = styled.img`
    width: 100%;
    height: 40%;
    border-radius: 8px;
`;

const Text = styled.p`
    font-size: 13px;
`;

const BulletList = styled.ul`
    padding-left: 20px;
`;

const BulletListItem = styled.li`
    margin-bottom: 8px;
    font-size: 13px;
`;

const Button = styled.button`
    background-color: #F3F5F7;
    color: #373b3f;
    border: none;
    border-radius: 4px;
    padding: 15px 20px;
    font-size: 13px;
    cursor: pointer;
    transition: background-color 0.3s ease;
    width: 100%;
    margin-top: 2rem;

    &:hover {
        border: 1px solid #D6DCEB;
    }
`;