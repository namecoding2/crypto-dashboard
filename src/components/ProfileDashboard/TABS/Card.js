import styled from "styled-components";
import {BsThreeDotsVertical} from "react-icons/bs";
import {useNavigate} from "react-router-dom";
import {formatCurrency} from "./formatCurrency";
import 'react-toastify/dist/ReactToastify.css';
import React, {useState} from "react";


export const AccountCard = ({user}) => {
    const navigate = useNavigate();

    const deposit = () => {
        navigate("/profile/deposit");
    }


    const withdrawal = () => {
        navigate("/profile/withdrawal");
    }


    const Invest = () => {
        navigate("/profile/investment");
    }

    const amount = user?.balance;
    const formattedAmount = formatCurrency(amount);

    return (<CardContainer>
        <AccountDetails>
            <span className="Demo">Real</span>
            <span># {user?.walletNumber}</span>
        </AccountDetails>

        <ButtonContainer>
            <Balance>{formattedAmount}
                {/*<span>.00 USD</span>*/}
            </Balance>
            <div>
                <GreyButton onClick={deposit}>Deposit</GreyButton>
                <GreyButton onClick={withdrawal}>Withdraw</GreyButton>
                <YellowButton onClick={Invest}>Trade</YellowButton>
                {/*<BsThreeDotsVertical className="dots"/>*/}
            </div>
        </ButtonContainer>
    </CardContainer>)
};


export const DemoCard = ({user}) => {

    const amount = user?.investBalance;
    const formattedAmount = formatCurrency(amount);

    return (<CardContainer>
        <AccountDetails>
            <span className="Demo">Investment</span>
            <span># {user?.walletNumber}</span>
        </AccountDetails>

        <ButtonContainer>
            <div>
                <GreyButton>Investment Balance</GreyButton>
                <Balance>{formattedAmount}</Balance>
            </div>

        </ButtonContainer>


    </CardContainer>);
};


export const ArchivedCard = ({user}) => {
    const [showCopiedMessage, setShowCopiedMessage] = useState(false);

    const code = user?.partnerCode
    const copyToClipboard = (code) => {
        if (navigator.clipboard && navigator.clipboard.writeText) {
            navigator.clipboard.writeText(code)
                .then(() => {
                    console.log("copied")
                    setShowCopiedMessage(true);
                    setTimeout(() => {
                        setShowCopiedMessage(false);
                    }, 3000);
                })
                .catch((error) => {
                    console.error("Failed to copy to clipboard:", error);
                });
        } else {
            console.error("Clipboard API not supported");
        }
    }


    return (
        <>
            {showCopiedMessage &&
                <span style={{
                    display:"flex",
                    justifyContent:"end",
                    fontSize: "10px",
                    fontWeight: "bold",
                    color: "green"
                }}>Partner Code {code} copied to clipboard!</span>}
            <CardContainer>
                <AccountDetails>
                    <span style={{fontWeight:"bold"}}> {user?.totalPartner || 0} </span>
                    <span>Total Partners</span>
                </AccountDetails>

                <ButtonContainer>
                    <Balance>
                        <span className="text">Your Partner Code</span> {user?.partnerCode} <br/>
                    </Balance>
                    <div>
                        <GreyButton onClick={() => copyToClipboard(code)}>Copy Code</GreyButton>
                    </div>

                </ButtonContainer>

            </CardContainer>
        </>

);
};


const CardContainer = styled.div`
    background-color: #FFFFFF;
    border-radius: 10px;
    border: 1px solid rgba(0, 0, 0, 0.1);
    padding: 20px;
    //box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    width: 100%;
    margin-top: 2.5rem;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    margin-bottom: 50px;
    
    @media (max-width: 768px) {
        width: 100%;
        color: #141D22;
    }
`;

const AccountDetails = styled.div`
    font-size: 12px;
    margin-bottom: 10px;

    span {
        margin: 6px;
        padding: 6px;
        background-color: #F3F5F7;
    }

    .Demo {
        background-color: #F0FBF5;
        color: #78d54e;
        //padding: 7px;
    }
`;


const Balance = styled.div`
    font-size: 20px;
    font-weight: bold;
    margin: 1rem 1rem;

    span {
        font-weight: bold;
        font-size: 13px;
    }

    .text {
        font-weight: 500;
        font-size: 13px;
        margin-left: 1rem;
    }

    //margin-top: 4rem;
`;

const ButtonContainer = styled.div`
    display: flex;
    justify-content: space-between;
    //margin-top: 2rem;
    width: 100%;
    margin-top: 3rem;

    .dots {
        font-size: 16px;
    }

    @media (max-width: 768px) {
        flex-direction: column;
    }
`;

const Button = styled.button`
    //flex-grow: 1;
    padding: 10px 20px;
    margin-right: 10px;
    margin-bottom: 10px;
    border: none;
    border-radius: 5px;
    cursor: pointer;

    @media (max-width: 768px) {
        margin-right: 8px;
        margin-top: 5px;
    }
`;

export const GreyButton = styled(Button)`
    background-color: #F3F5F7;
    //color: #00C076;
`;


const YellowButton = styled(Button)`
    background-color: #FFDE02;
`;