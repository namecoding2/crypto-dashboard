import React from 'react';
import styled from 'styled-components';


export const TextContainer = styled.div`
    display: flex;
    width: 100%;
    
`;

export const LeftElement = styled.span`
    //flex: 1;
    background-color: #f0f0f0;
    padding: 10px;
`;

export const RightElement = styled.span`
    //flex: 1;
    background-color: #e0e0e0;
    padding: 10px;
`;

// Component
const TwoElementFlexDiv = () => {
    return (<TextContainer>
        <LeftElement>
            Left Element
        </LeftElement>
        <RightElement>
            Right Element
        </RightElement>
    </TextContainer>);
};

export default TwoElementFlexDiv;
