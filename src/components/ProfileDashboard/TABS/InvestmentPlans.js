import React, {useState} from 'react';
import styled from 'styled-components';
import {
    AmountContainer,
    AuthButton,
    CardContainers,
    CardContent,
    Container,
    FieldContainer,
    FieldLabel,
    FormContainer,
    Text,
    TextWrapper
} from "./Deposit&WithdrawCard";
import Modal from "./Modal";
import {BiArrowBack} from "react-icons/bi";
import 'react-toastify/dist/ReactToastify.css';
import {toast, ToastContainer} from 'react-toastify';
import * as Yup from "yup";
import Loading from "../../../Loader/Loading";
import {useAuth} from "../../../contexts/AppContext";
import {InvestmentConfirmation} from "./TransactionHistory/confirmation";
import {formatCurrency} from "./formatCurrency";
import {useNavigate} from "react-router-dom";


const CardContainer = styled.div`
    flex: 0 0 auto;
    width: 360px;
    //width: 100%;
    height: 480px;
    color: #141D22;
    background-color: #ffffff;
    cursor: pointer;
    //border: 1px solid #FFDE02;
        // background-color: ${({active}) => (active ? 'rgba(255, 222, 2, 0.1)' : '#000000')};
    border: 1px solid ${({active}) => (active ? '#FFDE02' : '')};
    border-radius: 3px;
    padding: 20px;
    margin-right: 20px;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);

    &:hover {
        background-color: rgba(255, 222, 2, 0.1);
        color: black
    }

    @media screen and (max-width: 768px) {
        max-width: 100%;
        height: auto;
        &:hover {
            background-color: rgba(255, 222, 2, 0.1);
            color: black;
        }
    }
`;
const Title = styled.h2`
    font-size: 20px;
    margin-bottom: 10px;
    text-align: center;
`;

const SubTitle = styled.span`
    font-size: 12px;
    text-align: center;
    background-color: #FFDE02;
    color: #000000;
    border-radius: 10px;
    padding: 5px;
    margin-top: 9px;
    margin-left: 3rem;
    margin-right: 3rem;
    display: flex;
    justify-content: center;
`;

const Description = styled.div`
    font-size: 12px;
    margin-top: 10px;
    //white-space: pre-line;
    text-align: center;
`;

const DetailsList = styled.ul`
    list-style: none;
    padding: 0;
    margin-top: 3rem;
`;

const DetailItem = styled.li`
    font-size: 12px;
    border-bottom: 1px solid #E2E4E4;
    padding: 15px;
    display: flex;
    justify-content: space-between;
`;

const Label = styled.span`
    flex-grow: 1;
    text-transform: capitalize;
`;


export const InvestmentPlanCard = ({
                                       _id,
                                       planName,
                                       planType,
                                       desc,
                                       minimumInitialDeposit,
                                       maximumInitialDeposit,
                                       spread,
                                       commission,
                                       duration,
                                       planStatus
                                   }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const [active, setActive] = useState(false);

    const handleClick = () => {
        setActive(!active);
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };


    return (<div>
            {/*<ToastContainer/>*/}
            <CardContainer key={_id} _id={_id} active={active} onClick={handleClick}>
                <Title>{planName}</Title>
                <SubTitle>{planType}</SubTitle>
                <Description>{desc}</Description>
                <DetailsList>
                    <DetailItem>
                        <Label>Minimum Investment Deposit</Label>
                        <strong> ${minimumInitialDeposit}</strong>
                    </DetailItem>
                    <DetailItem>
                        <Label>Maximum Investment Deposit</Label>
                        <strong> ${maximumInitialDeposit}</strong>
                    </DetailItem>
                    <DetailItem>
                        <Label>Spread Profit</Label>
                        <strong>{spread} %</strong>
                    </DetailItem>
                    <DetailItem>
                        <Label>Commission</Label>
                        <strong>{commission}</strong>
                    </DetailItem>
                    <DetailItem>
                        <Label>Duration</Label>
                        <strong style={{textTransform: "capitalize"}}>{duration} Days</strong>
                    </DetailItem>
                </DetailsList>
            </CardContainer>

            <Modal isOpen={isModalOpen} onClose={closeModal}>
                <InvestmentDeposit
                    _id={_id}
                    planName={planName}
                    planType={planType}
                    desc={desc}
                    minimumInitialDeposit={minimumInitialDeposit}
                    maximumInitialDeposit={maximumInitialDeposit}
                    spread={spread}
                    commission={commission}
                    duration={duration}
                    planStatus={planStatus}
                    onClose={closeModal}
                />

            </Modal>
        </div>

    );
};


export const InvestmentDeposit = ({
                                      onClose,
                                      _id,
                                      planName,
                                      planType,
                                      desc,
                                      minimumInitialDeposit,
                                      maximumInitialDeposit,
                                      spread,
                                      duration,
                                  }) => {
    const {StartInvestment} = useAuth();
    const [submitted, setSubmitted] = useState(false);
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState({});
    const navigate = useNavigate();

    const [formData, setFormData] = useState({
        amount: '', spread: spread, duration: duration, investmentPlan: planName
    });


    const handleInvest = async () => {
        setLoading(true);
        try {
            await StartInvestment(formData.amount, formData.spread, formData.duration, formData.investmentPlan)
                .then((data) => {
                    console.log(data, "invest............")
                     if (data.message === "Investment started successfully") {
                        onClose()
                        return navigate(`/profile/success?amount=${data?.details?.amount}&message=${data?.details?.message}&transactionRef=${data?.details?.transactionRef}&transactionStatus=${data?.details?.transactionStatus}&date=${data?.details?.date}`);
                    }  else if (data?.message === 'Access Denied') {
                         toast.info("Login to Continue");
                         return navigate(`/`);
                     } else if(data.error) {
                         toast.info(data?.error);
                     }
                }).catch((error) => {
                    console.log(error, "network error")
                })
        } catch (err) {
            if (err instanceof Yup.ValidationError) {
                const validationErrors = {};
                err.inner.forEach(error => {
                    validationErrors[error.path] = error.message;
                    toast?.error(error.message)
                });
            }
        } finally {
            setLoading(false);
        }


    };

    const handleChange = async (e, countryName) => {
        const {name, value} = e?.target;
        setFormData({
            ...formData, [name]: value
        });
        if (errors[name] && value) {
            setErrors({...errors, [name]: ''});
        }
    };

    
    const handleSubmit = () => {
        // e?.preventDefault();
        try {
            // deleteToast('custom-id');
            if (formData.amount < minimumInitialDeposit || formData.amount > maximumInitialDeposit) {
                // toast(`Amount must be between $${minimumInitialDeposit} and $${maximumInitialDeposit}.`, {
                //     toastId: 'custom-id',
                //     autoClose: 2000,
                // });
            } else {
                setSubmitted(true);
            }
        } catch (error) {
            console.log(error)
        }

    };


    if (submitted) {
        return (<Container>
            <InvestmentConfirmation setSubmitted={setSubmitted}
                                    handleInvest={handleInvest}
                                    planName={planName}
                                    planType={planType}
                                    desc={desc} loading={loading}
                                    amount={formData.amount}
                                    minimumInitialDeposit={minimumInitialDeposit}
                                    maximumInitialDeposit={{maximumInitialDeposit}}
                                    spread={spread}
                                    duration={duration}

            />
        </Container>)
    }


    return (<Container _id={_id}>
            {/*<ToastContainer/>*/}
            <FormContainer>

                <h3 className='text' onClick={onClose}>
                    <BiArrowBack className='icon'/>
                </h3>

                <FieldContainer>
                    <FieldLabel>Investment Plan</FieldLabel>
                    <input
                        type="text"
                        defaultValue={planName}
                        disabled
                        style={{fontSize: "15px", fontWeight: "500"}}
                    />
                </FieldContainer>
                <FieldContainer>
                    <FieldLabel>Amount</FieldLabel>
                    <input
                        type="text"
                        value={formData.amount}
                        name="amount"
                        placeholder="0"
                        style={{fontSize: "15px", fontWeight: "500"}}
                        onChange={handleChange}
                    />
                    <span>{minimumInitialDeposit} - {maximumInitialDeposit} USD</span>
                </FieldContainer>
                <CardContainers>
                    <CardContent>
                        <Text>
                            Enter the amount you want to Invest. It should fall within the suggested
                            range and cannot
                            be more than available on your trading account or wallet.
                        </Text>
                    </CardContent>
                </CardContainers>
                <AmountContainer>
                    <p className="text">Investment Amount</p>
                    {formData.amount === "" ? "0 USD" : (parseFloat(formData.amount) || 0).toLocaleString('en-US', {
                        style: 'currency', currency: 'USD'
                    })}


                </AmountContainer>
                <AuthButton type="button" onClick={handleSubmit}>
                    Continue
                    {/*{loading ? <Loading/> : <span>Invest </span>}*/}
                </AuthButton>
            </FormContainer>
            <TextWrapper className='HT'>
                <p>
                    <strong>Investment Details</strong>
                </p>
                <p style={{textTransform: "capitalize"}}>Investment Duration:
                    <span style={{color: "#298551", fontWeight: "bold"}}> {duration}</span></p>
                <p style={{textTransform: "capitalize"}}>Spread profit:
                    <span style={{color: "#298551", fontWeight: "bold"}}> {spread}%</span></p>
            </TextWrapper>
        </Container>
    );
}


export const InvestmentWithdrawalForm = ({user, RedeemInvestment}) => {
    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState(false)
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        amount: '', toAccount: user.walletNumber,
    });


    const handleRedeemInvestment = async () => {
        setLoading(true);
        try {
            if (formData.amount >= user?.withdrawalLimit) {
                await RedeemInvestment(formData.amount, formData.toAccount)
                    .then((data) => {
                        console.log(data, "data...")
                         if (data?.status) {
                            toast.success(data.message);
                             return navigate(`/profile/success?amount=${data?.details?.amount}&message=${data?.details?.message}&transactionRef=${data?.details?.transactionRef}&transactionStatus=${data?.details?.transactionStatus}&date=${data?.details?.date}`);}
                         else if (data?.message === 'Access Denied') {
                             toast.info("Login to Continue");
                             return navigate(`/`);
                         } else if(data.error) {
                             toast.info(data?.error);
                         }
                    }).catch((error) => {
                        toast.error(error.message);
                    })
            } else {
                toast.error("minimum amount must exceed $100")
            }
        } catch (err) {
            if (err instanceof Yup.ValidationError) {
                const validationErrors = {};
                err.inner.forEach(error => {
                    validationErrors[error.path] = error.message;
                    toast.error(error.message)
                });
            }
        } finally {
            setLoading(false);
        }


    };

    const handleChange = async (e, countryName) => {
        const {name, value} = e.target;

        setFormData({
            ...formData, [name]: value
        });
        if (errors[name] && value) {
            setErrors({...errors, [name]: ''});
        }
    };


    const amount = user?.investBalance;
    const formattedAmount = formatCurrency(amount);

    return (<Container>
            <FormContainer>
                <FieldContainer>
                    <FieldLabel>From account</FieldLabel>
                    <div className="Account">
                        <div>
                            <span className="step-number">ST</span>
                            <span>{user?.walletNumber}</span>
                        </div>
                        <div>{formattedAmount} </div>
                    </div>
                </FieldContainer>

                <FieldContainer>
                    <FieldLabel>Amount</FieldLabel>
                    <input
                        type="text"
                        value={formData.amount}
                        name="amount"
                        placeholder="100"
                        style={{fontSize: "18px", fontWeight: "500"}}
                        onChange={handleChange}
                    />
                    <span>Minimum Withdrawal ${user?.withdrawalLimit}</span>
                </FieldContainer>


                <CardContainers>
                    <CardContent>
                        <Text>
                            Enter the amount you want to withdraw. It should fall within the
                            suggested range and cannot
                            be more than available on your trading account or wallet. The rest of
                            the funds may be
                            withdrawn using other methods.
                        </Text>
                    </CardContent>
                </CardContainers>

                <AmountContainer>
                    <p className="text">To be Withdraw</p>
                    {formData.amount === "" ? "0 USD" : (parseFloat(formData.amount) || 0).toLocaleString('en-US', {
                        style: 'currency', currency: 'USD'
                    })}

                </AmountContainer>

                <AuthButton type="button" onClick={handleRedeemInvestment}>   {loading ?
                    <Loading/> :
                    <span>continue </span>}</AuthButton>
            </FormContainer>
            <TextWrapper>
                <p>
                    <strong>Terms</strong>
                </p>
                <p>Average payment time: 1 hour</p>
                <p>Fee: 0%</p>
                <p>
                    <strong>FAQ</strong>
                </p>
                <p>How to deposit and withdraw with online bank transfers</p>
            </TextWrapper>
        </Container>
    );
}