import React, {useState} from 'react';
import styled from "styled-components";
import {toast, ToastContainer} from "react-toastify";
import * as Yup from "yup";
import {useAuth} from "../../contexts/AppContext";
// import {FormValidationSchema} from "../../validation/validation";
import Loading from "../../Loader/Loading";
import {useNavigate} from "react-router-dom";

const FileUploader = ({setIsModalOpen}) => {
    const [file, setFile] = useState(null);
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    // const [errors, setErrors] = useState({});

    const {UpLoadDocument} = useAuth();


    const handleFileInputChange = (event) => {
        const file = event.target.files?.[0];
        setFile(file);
        console.log(file, "file formData")
        if (file) {
            setFile(file);
        }
    };

    
    const handleUpLoad = async (e) => {
        e.preventDefault();
        setLoading(true);
      
        
        try {
            // await FormValidationSchema.validate(fileData);
            await UpLoadDocument(file)
                .then((data) => {
                    if (data.message === "Access Denied") {
                        return navigate(`/`);
                    } else if (data.status) {
                        console.log(data, "upload successful")
                        toast?.success(data?.message);
                    } else {
                        console.log(data.message, "error message")
                        toast?.info(data?.message);
                    }
                }).catch((error) => {
                    console.log(error, "Oops Something Went Wrong Please Try Again")
                    toast?.info("Oops Something Went Wrong Please Try Again");
                })
        } catch (error) {
            if (error instanceof Yup.ValidationError) {
                const validationErrors = {};
                error.inner.forEach(error => {
                    validationErrors[error.path] = error.message;
                });
                toast?.info(error.message);
            }
        } finally {
            setLoading(false);
            // setIsModalOpen(false)

        }
    };


    return (<div>
        {/*<ToastContainer/>*/}
        <ProfileUpdateContainer>
            <ProfileUpdateForm>
                <InputField
                    placeholder="phone"
                    name="phone"
                    type="file"
                    accept=".png, .jpeg, .jpg"
                    onChange={handleFileInputChange}
                    // value={formData?.phone}
                    // onChange={handleChange}
                    required/>
                {/*{errors.phone && !formData.phone && <div className="error">{errors.phone}</div>}*/}

                <ButtonContainer>
                    {/*<span onClick={onCancel}>click here to cancel?</span>*/}
                    <SubmitButton type="button" onClick={handleUpLoad}>
                        {loading ? <Loading/> : <> verify document</>}
                    </SubmitButton>
                </ButtonContainer>
                {/*<SubmitButton type="submit" onClick={onCancel}>Update Profile</SubmitButton>*/}
            </ProfileUpdateForm>
        </ProfileUpdateContainer>
    </div>);
};


const ProfileUpdateContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: start;
    padding: 1rem;
    text-align: start;
    width: 100%;

    h2 {
        font-size: 20px;
        margin-bottom: 1rem;
    }

    p {
        font-size: 12px;
    }
`;

const ProfileUpdateForm = styled.div`
    width: 100%;
    //max-width: 400px; /* Set maximum width for larger screens */
`;

const InputField = styled.input`
    border: 1px solid #558c35;
    background: transparent;
    outline: none;
    color: #1C2127;
    font-size: 12px;
    border-radius: 5px;
    padding: 20px 20px;
    width: 100%;
`;


const ButtonContainer = styled.div`
    margin-top: 2rem;
    display: flex;
    justify-content: space-between;
    align-items: center;

    span {
        font-size: 12px;
        color: #B9A62CFF;
        cursor: pointer;
    }
`


const SubmitButton = styled.button`
    padding: 10px 20px;
    background-color: #FFCF01;
    color: #000000;
    border: none;
    border-radius: 5px;
    cursor: pointer;
`;


export default FileUploader;
