import React, {useState} from "react";
import {useNavigate} from 'react-router-dom'
import {useAuth} from "../../contexts/AppContext";
import {LoginSchema} from "../../validation/validation";
import * as Yup from "yup";
import Loading from "../../Loader/Loading";
import 'react-toastify/dist/ReactToastify.css';
import {toast } from 'react-toastify';

const Login = () => {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [errors, setErrors] = useState({});
    const {login, isLoggedIn} = useAuth();

    const [formData, setFormData] = useState({
        email: '', password: '',
    });


    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const handleLogin = async () => {
        // e.preventDefault();
        setLoading(true);
        try {
            await LoginSchema.validate(formData);
            await login(formData.email, formData.password)
                .then((data) => {
                    if (data.message === "Required 2-step verification") {
                        toast.info(data.message);
                        return navigate(`/security?email=${formData.email}`);
                    } else if (data.status) {
                       // if(isLoggedIn === true){
                        toast.success(data.message);
                        return navigate('/profile')
                       // }
                    } else {
                        toast.info(data.message);
                        return navigate('/')
                    }
                }).catch((error) => {
                    toast.info("Oops Something Went Wrong Please Try Again");
                })
        } catch (error) {
            if (error instanceof Yup.ValidationError) {
                const validationErrors = {};
                error.inner.forEach(error => {
                    validationErrors[error.path] = error.message;
                });
                toast.info(error.message);
            }
        } finally {
            setLoading(false);
        }
    };

    const handleChange = (e, countryName) => {
        const {name, value} = e.target;
        setFormData({
            ...formData, [name]: value
        });
        if (errors[name] && value) {
            setErrors({...errors, [name]: ''});
        }
    };


    return (<div>
        <div className="auth-container__form">
            <div>
                <label>Your Email Address</label>
                <input type="email"
                       placeholder="Email"
                       name="email"
                       value={formData.email}
                       onChange={handleChange}
                       required/>
                {errors.email && !formData.email && <div className="error">{errors.email}</div>}
            </div>


            <div>
                <label>Password</label>
                <div>
                    <input
                        type={showPassword ? 'text' : 'password'}
                        placeholder="Password"
                        name="password"
                        value={formData.password}
                        onChange={handleChange}
                        required
                    />
                </div>
                {errors.password && !formData.password &&
                    <div className="error">{errors.password}</div>}
            </div>
            <div className="checkboxToggle">
                <label>Show Password</label>
                <input type="checkbox" placeholder="password" id="password"
                       className="checkbox_input" style={{cursor: "pointer"}}
                       onClick={togglePasswordVisibility}
                       required/>
            </div>

            <button type="button" onClick={handleLogin} disabled={loading}>
                {loading ? <Loading/> : <span>Continue</span>}
            </button>
        </div>
    </div>)
}

export default Login