import React from 'react'
// import logo from "../../assets/images/Fictional company logo.svg";
// import logo from "../../assets/logo.svg";
import logo from "../../assets/unity.png";
import styled from "styled-components";
import {AiOutlineGlobal} from "react-icons/ai";

const AuthHeader = ({OpenSidebar}) => {

    return (<TopHeader className='header'>
            {/*<header className='header'>*/}
            {/*<div className='menu-icon'>*/}
            {/*    <GiTrade className='open-icon' onClick={OpenSidebar}/>*/}
            {/*</div>*/}
            <div className='header-left'>
                {/*<BsSearch className='icon'/>*/}
                <img
                    src={logo}
                    alt="fictional logo"
                    className='icon'
                    style={{width: "30%",}}
                />
            </div>
            <div className='header-right'>
                <AiOutlineGlobal className='Nicon' style={{fontSize: "25px"}}/>

            </div>
            {/*</header>*/}
        </TopHeader>


    )

}

export default AuthHeader

const TopHeader = styled.div`
    padding-left: 7rem;
    padding-right: 7rem;
    @media (max-width: 768px) {
        padding-left: 1rem;
        padding-right: 1rem;
    }
`
