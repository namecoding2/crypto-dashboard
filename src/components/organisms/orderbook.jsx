import React, {useState} from 'react'
import styles from './organisms.module.css'
import Filter from '../molecules/filter'
import Table from '../molecules/table'
import atom from '../atoms/atoms.module.css'
import molecule from "../molecules/molecules.module.css";

const Orderbook = ({style, orderBookData, pairTicker}) => {


    // const activeStyleTab = {
    //     borderRadius: '8px',
    //     color: ' #FFF',
    //     background: '#21262C',
    //     boxShadow: '0px 3px 1px 0px rgba(0, 0, 0, 0.04), 0px 3px 8px 0px rgba(0, 0, 0, 0.12)'
    // }
    //
    // const list = [{
    //     title: "Order Book", active: true
    // }, {
    //     title: "Recent Trades", active: false
    // }]
    //
    // // do nothing
    // const handle = () => null

    const [activeTab, setActiveTab] = useState('order');
    const handleTabClicks = (tab) => {
        setActiveTab(tab.toLowerCase());
    };

    return (<div className={styles.orderbook} style={style}>
        <div className={atom.hide}>
            {/*<Tabs tabs={list} handleClick={handle}/>*/}


            <ul className={molecule.segmentedTabs}>
                <li className={activeTab === 'order' ? `${molecule.segmentedTabs__links} ${molecule.active}` : `${molecule.segmentedTabs__links} `}
                    onClick={() => handleTabClicks('order')}>
                    Order
                </li>

                {/*<li className={activeTab === 'recent' ? `${molecule.segmentedTabs__links} ${molecule.active}` : `${molecule.segmentedTabs__links} `}*/}
                {/*    onClick={() => handleTabClicks('recent')}>*/}
                {/*    Recent*/}
                {/*</li>*/}

            </ul>


        </div>

        {activeTab === 'order' && <div>
            {/* <!-- filter --> */}
            <Filter/>
            {/* <!-- Table --> */}
            <Table orderBookData={orderBookData} pairTicker={pairTicker}/>
        </div>}

        {activeTab === 'recent' && <div>recent content here</div>}

    </div>)
}

export default Orderbook