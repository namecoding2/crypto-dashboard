import React, {useEffect} from "react";
import {Route, Routes, useLocation, useNavigate,} from 'react-router-dom';
import "react-toastify/dist/ReactToastify.css";
import './App.css';
import Index from "./pages/profile";
import Setting from "./pages/profile/setting";
import Investment from "./pages/profile/investment";
import Auth from './pages/auth';
import Dashboard from './pages/dashboard';
import Deposit from "./pages/profile/deposit";
import Transactions from "./pages/profile/transactions";
import Portfolio from "./pages/profile/portfolio";
import WithdrawFunds from "./pages/profile/wtihdrawal";
import {useAuth, AuthProvider} from "./contexts/AppContext";
import ResetPasswords from "./pages/resetpassword";
import ResetPasswordOTPs from "./pages/resetPasswordOtp";
import UpdatePassword from "./pages/updatePassword";
import SecurityLayout from "./pages/security";
import Congratulation from "./pages/congratulation";
import 'react-toastify/dist/ReactToastify.css';
import Success from "./pages/profile/success";


function App() {
    const navigate = useNavigate();
    const location = useLocation();
    // const token = localStorage.getItem('token');
    const {user, isLoggedIn, setIsLoggedIn, setToken, setUser, token} = useAuth();


    useEffect(() => {
        // const token = localStorage.getItem('token');
        window.ResizeObserver = null;
        if (token) {
           setToken(token);
           setIsLoggedIn(true);
                // navigate('/profile');
               // console.log(token, `Token is still valid & isLoggedIn is ${isLoggedIn}`);
        }else{
            setIsLoggedIn(false);
            setToken(null);
            setUser(null); // Clear user data
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            localStorage.removeItem('isLoggedIn');
            localStorage.removeItem('InvestmentPlan');
            localStorage.removeItem('investmentHistory');
            navigate('/');
        }
    }, [token]);
    // }, [token, setUser, setToken, navigate]);

    
    return (<div>
   {/* <AuthProvider> */}
        <Routes>
            {isLoggedIn && (<React.Fragment>
              <Route path='/congratulation' element={<Congratulation/>}/>
                            <Route path='/profile/*' element={<Index/>}/>
                            <Route path='/dashboard' element={<Dashboard/>}/>
                            <Route path='/profile/deposit' element={<Deposit/>}/>
                            <Route path='/profile/withdrawal' element={<WithdrawFunds/>}/>
                            <Route path='/profile/transactions' element={<Transactions/>}/>
                            <Route path='/profile/setting' element={<Setting/>}/>
                            <Route path='/profile/investment' element={<Investment/>}/>
                            <Route path='/profile/portfolio' element={<Portfolio/>}/>
                            <Route path='/profile/success' element={<Success/>}/>
                    </React.Fragment>)}


            {/*: (*/}
            {/*<React.Fragment>*/}
            {/*    /!* <Route path='/*' element={<Auth/>}/> *!/*/}
            {/*    <Route path='/*' element={<Auth/>}/>*/}
            {/*    <Route path='/resetpassword' element={<ResetPasswords/>}/>*/}
            {/*    <Route path='/resetpasswordOtp' element={<ResetPasswordOTPs/>}/>*/}
            {/*    <Route path='/updatePassword' element={<UpdatePassword/>}/>*/}
            {/*    <Route path='/security' element={<SecurityLayout/>}/>*/}
            {/*</React.Fragment>*/}
            {/*)*/}
            <Route path='/*' element={<Auth/>}/>
            <Route path='/resetpassword' element={<ResetPasswords/>}/>
            <Route path='/resetpasswordOtp' element={<ResetPasswordOTPs/>}/>
            <Route path='/updatePassword' element={<UpdatePassword/>}/>
            <Route path='/security' element={<SecurityLayout/>}/>
        </Routes>
     {/* </AuthProvider> */}
    </div>)

}

export default App;
